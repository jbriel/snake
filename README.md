# Snake

Welcome to my first ever built game. I have followed a guide online and read documentation in order to bring this game to myself and you if you want to play snake.
## Installation



```bash
pip install -r requirements.txt
```

## Usage

```python
from pygame.locals import *
from random import randint
import pygame, sys, os
import time
```

## Contributing
Contributing is allowed. Dont be afraid to make changed. :-)