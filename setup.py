try:
    from setuptools import setuptools
except ImportError:
    from disutils.core import setup

config = {
  'descritption': 'a game of hangman',
  'author': 'Julien Briel',
  'url': 'place holder for git repo',
  'download_url': 'Where to download it',
  'author_email': 'julien_briel@hotmail.com',
  'version': '1.0',
  'install_requires': ['n/a'],
  'packages': ['pygame', 'time', 'sys', 'pygame.locals'],
  'scripts': [],
  'name': 'Snake Game'
}

setup()